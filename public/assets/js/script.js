function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  function toggleCardContent(card) {
    if ('ontouchstart' in window || navigator.maxTouchPoints) {
        card.classList.toggle('active');
    } else {
        if (card.classList.contains('active')) {
            card.classList.remove('active');
        } else {
            // Close all other open cards before opening this one
            var allCards = document.querySelectorAll('.card');
            allCards.forEach(function(item) {
                item.classList.remove('active');
          });
          card.classList.add('active');
      }
  }
}